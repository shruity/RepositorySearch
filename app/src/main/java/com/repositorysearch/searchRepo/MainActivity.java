package com.repositorysearch.searchRepo;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.repositorysearch.DataModel;
import com.repositorysearch.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    Context context;
    EditText et_searchbox;
    TextView tv_search;
    ImageView iv_filter, iv_search;
    LinearLayout ll_btn, filters_layout;
    Button bt_clearFilters;
    TextView tv_sizeFilter, tv_forks;
    Spinner limitSizeSpinner, sizeDimenSpinner, forksLimitSpinner, languageIncludeSpinner, sortFilter, orderBy;
    EditText et_sizeOfRepo, et_forksSize, et_languageName;
    RecyclerView rv_list;
    String searchText;
    int page = 0;
    TextView tv_pageNo;
    Button bt_prev,bt_next;
    JSONArray items;
    String total_count,incomplete_results;
    ArrayList<DataModel> datalist;
    DataAdapterRepositoryList adapter;
    Boolean showFilters = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        context                  = this;
        et_searchbox             = (EditText) findViewById(R.id.et_searchbox);
        et_sizeOfRepo            = (EditText) findViewById(R.id.et_size);
        et_forksSize             = (EditText) findViewById(R.id.et_forksSize);
        et_languageName          = (EditText) findViewById(R.id.et_languageName);
        tv_search                = (TextView) findViewById(R.id.tv_search);
        tv_pageNo                = (TextView) findViewById(R.id.tv_page);
        iv_filter                = (ImageView) findViewById(R.id.iv_filter);
        iv_search                = (ImageView) findViewById(R.id.iv_search);
        ll_btn                   = (LinearLayout) findViewById(R.id.ll_btn);
        filters_layout           = (LinearLayout) findViewById(R.id.filters_layout);
        bt_prev                  = (Button) findViewById(R.id.bt_prev);
        bt_next                  = (Button) findViewById(R.id.bt_next);
        bt_clearFilters          = (Button) findViewById(R.id.bt_clearFilters);
        rv_list                  = (RecyclerView) findViewById(R.id.rv_list);
        limitSizeSpinner         = (Spinner) findViewById(R.id.limitSizeSpinner);
        sizeDimenSpinner         = (Spinner) findViewById(R.id.sizeDimenSpinner);
        forksLimitSpinner        = (Spinner) findViewById(R.id.forksLimitSpinner);
        languageIncludeSpinner   = (Spinner) findViewById(R.id.LanguageIncludeSpinner);
        sortFilter               = (Spinner) findViewById(R.id.sortFilter);
        orderBy                  = (Spinner) findViewById(R.id.orderBy);
        datalist                 = new ArrayList<>();

        //Add value to all the spinners=========================================================================
        String[] a = new String[]{"Less than", "Greater than"};
        ArrayAdapter<String> adapter1 = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item, a);
        limitSizeSpinner.setAdapter(adapter1);

        String[] b = new String[]{"KB", "MB", "GB"};
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item, b);
        sizeDimenSpinner.setAdapter(adapter2);

        String[] c = new String[]{"More than", "Less than"};
        ArrayAdapter<String> adapter3 = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item, c);
        forksLimitSpinner.setAdapter(adapter3);

        String[] d = new String[]{"Include Only", "Exclude"};
        ArrayAdapter<String> adapter4 = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item, d);
        languageIncludeSpinner.setAdapter(adapter4);

        String[] e = new String[]{"Best Match", "stars", "forks", "updated"};
        ArrayAdapter<String> adapter5 = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item, e);
        sortFilter.setAdapter(adapter5);

        String[] f = new String[]{"Desc", "Asc"};
        ArrayAdapter<String> adapter6 = new ArrayAdapter<>(this,android.R.layout.simple_spinner_dropdown_item, f);
        orderBy.setAdapter(adapter6);
        //======================================================================================================


        //show and hide filters
        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide keyboard
                View view = MainActivity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }

                if (!showFilters) {
                    clearAllFilter();
                    filters_layout.setVisibility(View.VISIBLE);
                    showFilters = true;
                } else {
                    clearAllFilter();
                    filters_layout.setVisibility(View.GONE);
                    showFilters = false;
                }
            }
        });

        bt_clearFilters.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //hide keyboard
                View view = MainActivity.this.getCurrentFocus();
                if (view != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                clearAllFilter();
            }
        });


        iv_search.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {

                View v = MainActivity.this.getCurrentFocus();
                if (showFilters)
                    filters_layout.setVisibility(View.GONE);
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }
                et_searchbox.clearFocus();


                if (!et_searchbox.getText().toString().trim().isEmpty()){
                    searchText = et_searchbox.getText().toString().trim();
                }

                String x = et_sizeOfRepo.getText().toString();
                String x3 = "";
                String x4 = "";
                if(!x.isEmpty()){
                    String x1 = limitSizeSpinner.getSelectedItem().toString();
                    if(x1.equals("Less than")){
                        x4 += "<";
                    }else{
                        x4 += ">";
                    }

                    int x2 = sizeDimenSpinner.getSelectedItemPosition();
                    if(x2==1){
                        x3 += String.valueOf(Integer.parseInt(x)*1024);
                    }else if(x2==2){
                        x3 += String.valueOf(Integer.parseInt(x)*1024*1024);
                    }else{
                        x3 += x;
                    }
                    searchText+=("+size:"+x4+x3);
                }

                //manipulation for forks filter
                String y = et_forksSize.getText().toString();
                if(!y.isEmpty()){
                    String y1 = forksLimitSpinner.getSelectedItem().toString();
                    if(y1.equals("More than")){
                        searchText+="+forks:>";
                    }else{
                        searchText+="+forks:<";
                    }

                    searchText+=y;
                }

                //manipulation for language filter
                String z = et_languageName.getText().toString();
                if(!z.isEmpty()){
                    String z1 = languageIncludeSpinner.getSelectedItem().toString();
                    if(z1.equals("Exclude")){
                        searchText+=("+-language:"+z);

                    }else{
                        searchText+=("+language:"+z);
                    }
                }

                //manipulation for SortBy filter
                String w = sortFilter.getSelectedItem().toString();
                if(!w.equals("Best Match")){
                    searchText+=("&sort="+w);
                }

                //manipulation for Order filter
                String w1 = orderBy.getSelectedItem().toString();
                if(w1.equals("Asc")){
                    searchText+="&order=asc";
                }else{
                    searchText+="&order=desc";
                }

                searchText+="&per_page=10";
                if (isInternetConnected(context)){
                    datalist.clear();
                    new Atask().execute(searchText+"&page="+String.valueOf(page + 1));
                }
                else
                    Toast.makeText(MainActivity.this, "No Internet Connection!",Toast.LENGTH_SHORT).show();
            }
        });

        et_searchbox.setOnEditorActionListener(new OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                View v = MainActivity.this.getCurrentFocus();
                if (v != null) {
                    InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                }


                if (!et_searchbox.getText().toString().trim().isEmpty()){
                    searchText = et_searchbox.getText().toString().trim();
                }

                searchText+="&order=desc";
                searchText+="&per_page=10";
                if (isInternetConnected(context)){
                    new Atask().execute(searchText+"&page="+String.valueOf(page + 1));
                }
                return true;
            }
        });

        bt_prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datalist.clear();
                page -= 1;
                new Atask().execute(searchText + "&page=" + String.valueOf(page + 1));
            }
        });
        bt_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                datalist.clear();
                page += 1;
                new Atask().execute(searchText + "&page=" + String.valueOf(page + 1));
            }
        });

        if (datalist.isEmpty()) {
            tv_search.setVisibility(View.VISIBLE);
            rv_list.setVisibility(View.GONE);
            ll_btn.setVisibility(View.GONE);
        }
        else {
            tv_search.setVisibility(View.GONE);
            rv_list.setVisibility(View.VISIBLE);
            ll_btn.setVisibility(View.GONE);
        }

    }
    private void clearAllFilter() {
        et_sizeOfRepo.setText("");
        et_forksSize.setText("");
        et_languageName.setText("");
        sortFilter.setSelection(0);
        orderBy.setSelection(0);
    }
    public static boolean isInternetConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

   public class Atask extends AsyncTask<String,Void,Void> {
        private ProgressDialog pDialog;
        boolean apiLimitExceeded = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {
            HttpURLConnection urlConnection;
            URL url;
            InputStream inputStream;
            String response="";

            try{
                url = new URL("https://api.github.com/search/repositories?q="+params[0]);
                Log.e("url valeu", url.toString());
                urlConnection = (HttpURLConnection) url.openConnection();

                //set request type
                urlConnection.setRequestMethod("GET");

                urlConnection.setDoInput(true);
                urlConnection.connect();
                //check for HTTP response
                int httpStatus = urlConnection.getResponseCode();
                Log.e("httpstatus", "The response is: " + httpStatus);

                //if HTTP response is 200 i.e. HTTP_OK read inputstream else read errorstream
                if (httpStatus != HttpURLConnection.HTTP_OK) {
                    inputStream = urlConnection.getErrorStream();
                    Map<String, List<String>> map = urlConnection.getHeaderFields();
                    System.out.println("Printing Response Header...\n");
                    for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                        System.out.println(entry.getKey()
                                + " : " + entry.getValue());
                    }
                }
                else {
                    inputStream = urlConnection.getInputStream();
                }

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String temp;
                while((temp = bufferedReader.readLine())!=null){
                    response+=temp;
                }
                Log.e("webapi json object",response);


                if(response.contains("API rate limit exceeded")){
//                    items= new JSONArray();
//                    total_count = "0";
                    apiLimitExceeded =true;
                }else {
                    //convert data string into JSONObject
                    JSONObject obj = (JSONObject) new JSONTokener(response).nextValue();
                    items = obj.getJSONArray("items");

                    total_count = obj.getString("total_count");
                    incomplete_results = obj.getString("incomplete_results");

                    for (int i =0;i<items.length(); i++){
                        JSONObject jsonObject = items.getJSONObject(i);
                        DataModel dataModel = new DataModel();
                        //dataModel.setAvatar_url(jsonObject.getString("avatar_url"));
                        dataModel.setName(jsonObject.getString("name"));
                        dataModel.setFull_name(jsonObject.getString("full_name"));
                        dataModel.setWatchers_count(jsonObject.getInt("watchers_count"));
                        dataModel.setCommit_count(jsonObject.getString("forks_count"));
                        dataModel.setProject_url(jsonObject.getString("html_url"));
                        dataModel.setDescription(jsonObject.getString("description"));

                        Log.e("description",jsonObject.getString("description"));

                        JSONObject ownerjsonobject = jsonObject.getJSONObject("owner");
                        dataModel.setAvatar_url(ownerjsonobject.getString("avatar_url"));


                        datalist.add(dataModel);
                    }
                }

                urlConnection.disconnect();
            } catch (MalformedURLException | ProtocolException | JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(!apiLimitExceeded){
                //apiLimitError.setVisibility(View.INVISIBLE);
                //setResultListView();
                Collections.sort(datalist, new Comparator<DataModel>() {
                    public int compare(DataModel s1, DataModel s2) {
                        return s2.getWatchers_count()-(s1.getWatchers_count());
                    }
                });

                tv_pageNo.setText("Page " + String.valueOf(page + 1));
                tv_search.setVisibility(View.GONE);
                rv_list.setVisibility(View.VISIBLE);
                ll_btn.setVisibility(View.VISIBLE);
                adapter = new DataAdapterRepositoryList(context,datalist);
                rv_list.setAdapter(adapter);
            }else{
                //repoListView.setAdapter(new ArrayAdapter<>(getBaseContext(), android.R.layout.simple_list_item_1, new ArrayList<>()));
                //apiLimitError.setVisibility(View.VISIBLE);
                //count.setText("API rate Limit Error!!Try after some time!");
            }
            pDialog.dismiss();
        }
    }

    public void onBackPressed() {
            new AlertDialog.Builder(this,R.style.MyDialogTheme)
                    .setMessage("Are you sure you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            moveTaskToBack(true);
                            exitAppMethod();
                            finish();
                            System.exit(0);
                        }
                    })
                    .setNegativeButton("No", null)
                    .show();
    }

    public void exitAppMethod(){
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        finishAffinity();
        startActivity(intent);
    }

}
