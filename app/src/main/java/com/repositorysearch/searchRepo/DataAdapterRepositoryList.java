package com.repositorysearch.searchRepo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.repositorysearch.DataModel;
import com.repositorysearch.R;
import com.repositorysearch.repoDetails.RepoDetailsActivity;

import java.util.List;

public class DataAdapterRepositoryList extends RecyclerView.Adapter<DataAdapterRepositoryList.MyViewHolder> {

    private List<DataModel> dataModelList;
    private Context context;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_repo_name,tv_full_name;
        private TextView tv_watcher_count,tv_commit_count;
        private ImageView iv_avatar_image;
        private LinearLayout linearLayout;
        CardView cardView;

        public MyViewHolder(View view) {
            super(view);
            tv_repo_name        = (TextView) view.findViewById(R.id.tv_repo_name);
            tv_full_name        = (TextView) view.findViewById(R.id.tv_full_name);
            tv_watcher_count    = (TextView) view.findViewById(R.id.tv_watcher_count);
            tv_commit_count     = (TextView) view.findViewById(R.id.tv_commit_count);
            iv_avatar_image     = (ImageView) view.findViewById(R.id.iv_avatar_image);
            linearLayout        = (LinearLayout) view.findViewById(R.id.ll_list);
            cardView            = (CardView) view.findViewById(R.id.card_view);

        }
    }

    public DataAdapterRepositoryList(Context context, List<DataModel> dataModelList) {
        this.dataModelList       = dataModelList;
        this.context             = context;
        activity                 = (Activity) context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.data_adapter_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DataModel dataModel = dataModelList.get(position);

        Glide.with(context).load(dataModel.getAvatar_url()).error(R.drawable.avatar_image).placeholder(R.drawable.avatar_image)
                .into(holder.iv_avatar_image);

        holder.tv_repo_name.setText("Name: "+dataModel.getName());
        holder.tv_full_name.setText("Full Name : "+dataModel.getFull_name());
        holder.tv_watcher_count.setText("Watcher's count: "+dataModel.getWatchers_count());
        holder.tv_commit_count.setText("Forks count: "+dataModel.getCommit_count());

        holder.cardView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,RepoDetailsActivity.class);
                int[] screenLocation = new int[2];
                holder.iv_avatar_image.getLocationOnScreen(screenLocation);
                intent.putExtra("left", screenLocation[0]).
                        putExtra("top", screenLocation[1]).
                        putExtra("width", holder.iv_avatar_image.getWidth()).
                        putExtra("height", holder.iv_avatar_image.getHeight());
                intent.putExtra("repoName",dataModel.getFull_name());
                intent.putExtra("avatarImage",dataModel.getAvatar_url());
                intent.putExtra("projectUrl",dataModel.getProject_url());
                intent.putExtra("description",dataModel.getDescription());
                context.startActivity(intent);
                activity.overridePendingTransition(0,0);
            }
        });

    }


    public int item_id(int pos){
        DataModel rm=dataModelList.get(pos);
        int id= Integer.parseInt(rm.getId());
        return id;
    }

    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

}

