package com.repositorysearch.repoDetails;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.repositorysearch.DataModel;
import com.repositorysearch.R;
import com.repositorysearch.contributorsDetails.ContributorsDetailsActivity;

import java.util.List;

public class ContributorListAdapter extends RecyclerView.Adapter<ContributorListAdapter.MyViewHolder> {

    private List<DataModel> dataModelList;
    private Context context;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_contributor_name;
        private ImageView iv_avatar_image;
        private LinearLayout linearLayout;
        CardView cardView;

        public MyViewHolder(View view) {
            super(view);

            tv_contributor_name = (TextView) view.findViewById(R.id.tv_contributor_name);
            iv_avatar_image     = (ImageView) view.findViewById(R.id.iv_avatar_image);
            linearLayout        = (LinearLayout) view.findViewById(R.id.ll_list);
            cardView            = (CardView) view.findViewById(R.id.card_view);

        }
    }

    public ContributorListAdapter(Context context, List<DataModel> dataModelList) {
        this.dataModelList       = dataModelList;
        this.context             = context;
        activity                 = (Activity) context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contributors_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DataModel dataModel = dataModelList.get(position);

        Glide.with(context).load(dataModel.getContributor_avatar_url()).asBitmap().error(R.drawable.avatar_image)
                .placeholder(R.drawable.avatar_image).into(holder.iv_avatar_image);

        holder.tv_contributor_name.setText(dataModel.getContributor_name());

        holder.cardView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,ContributorsDetailsActivity.class);
                int[] screenLocation = new int[2];
                holder.iv_avatar_image.getLocationOnScreen(screenLocation);
                intent.putExtra("left", screenLocation[0]).
                        putExtra("top", screenLocation[1]).
                        putExtra("width", holder.iv_avatar_image.getWidth()).
                        putExtra("height", holder.iv_avatar_image.getHeight());
                intent.putExtra("contributorName",dataModel.getContributor_name());
                intent.putExtra("contributorAvatarImage",dataModel.getContributor_avatar_url());
                context.startActivity(intent);
                activity.overridePendingTransition(0,0);
            }
        });

    }


    public int item_id(int pos){
        DataModel rm=dataModelList.get(pos);
        int id= Integer.parseInt(rm.getId());
        return id;
    }

    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

}

