package com.repositorysearch.contributorsDetails;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;

import com.repositorysearch.DataModel;
import com.repositorysearch.R;
import com.repositorysearch.repoDetails.RepoDetailsActivity;

import java.util.List;

public class ContributorRepoAdapter extends RecyclerView.Adapter<ContributorRepoAdapter.MyViewHolder> {

    private List<DataModel> dataModelList;
    private Context context;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tv_repo_name;
        CardView cardView;

        public MyViewHolder(View view) {
            super(view);

            tv_repo_name = (TextView) view.findViewById(R.id.tv_repo_name);
            cardView     = (CardView) view.findViewById(R.id.card_view);

        }
    }

    public ContributorRepoAdapter(Context context, List<DataModel> dataModelList) {
        this.dataModelList       = dataModelList;
        this.context             = context;
        activity                 = (Activity) context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.contributors_repo_adapter, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final DataModel dataModel = dataModelList.get(position);

        holder.tv_repo_name.setText(dataModel.getFull_name());

        holder.cardView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context,RepoDetailsActivity.class);
                intent.putExtra("left", 0).
                        putExtra("top", 0).
                        putExtra("width", 0).
                        putExtra("height", 0);
                intent.putExtra("repoName",dataModel.getFull_name());
                intent.putExtra("avatarImage",dataModel.getAvatar_url());
                intent.putExtra("projectUrl",dataModel.getProject_url());
                intent.putExtra("description",dataModel.getDescription());
                context.startActivity(intent);
                activity.overridePendingTransition(R.anim.pull_in_right, R.anim.push_out_left);
            }
        });

    }


    public int item_id(int pos){
        DataModel rm=dataModelList.get(pos);
        int id= Integer.parseInt(rm.getId());
        return id;
    }

    @Override
    public int getItemCount() {
        return dataModelList.size();
    }

}

