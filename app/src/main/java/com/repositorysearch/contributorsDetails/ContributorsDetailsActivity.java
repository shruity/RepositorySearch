package com.repositorysearch.contributorsDetails;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.repositorysearch.DataModel;
import com.repositorysearch.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.repositorysearch.searchRepo.MainActivity.isInternetConnected;

public class ContributorsDetailsActivity extends AppCompatActivity {

    Context context;
    TextView tv_heading;
    ImageView iv_avatar_image, iv_back;
    RecyclerView rv_contributors_repo;
    ContributorRepoAdapter adapter;
    String contributorName, contributorAvatarUrl;
    ArrayList<DataModel> contributorsRepoList;

    int thumbnailTop;
    int thumbnailLeft;
    int thumbnailWidth;
    int thumbnailHeight;
    private static final int ANIM_DURATION = 200;
    private int mLeftDelta;
    private int mTopDelta;
    private float mWidthScale;
    private float mHeightScale;
    ColorDrawable colorDrawable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_contributors_details);

        context             = this;
        tv_heading          = (TextView) findViewById(R.id.tv_heading);
        iv_avatar_image     = (ImageView) findViewById(R.id.iv_avatar_image);
        iv_back             = (ImageView) findViewById(R.id.im_backbutton);
        rv_contributors_repo= (RecyclerView) findViewById(R.id.rv_contributors_repo);
        contributorsRepoList= new ArrayList<>();

        if (getIntent().getExtras()!=null){
            contributorName         = getIntent().getStringExtra("contributorName");
            contributorAvatarUrl    = getIntent().getStringExtra("contributorAvatarImage");
            thumbnailTop            = getIntent().getIntExtra("top",0);
            thumbnailLeft           = getIntent().getIntExtra("left",0);
            thumbnailWidth          = getIntent().getIntExtra("width",0);
            thumbnailHeight         = getIntent().getIntExtra("height",0);
        }

        if (savedInstanceState == null) {
            ViewTreeObserver observer = iv_avatar_image.getViewTreeObserver();
            observer.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {

                @Override
                public boolean onPreDraw() {
                    iv_avatar_image.getViewTreeObserver().removeOnPreDrawListener(this);

                    // Figure out where the thumbnail and full size versions are, relative
                    // to the screen and each other
                    int[] screenLocation = new int[2];
                    iv_avatar_image.getLocationOnScreen(screenLocation);
                    mLeftDelta = thumbnailLeft - screenLocation[0];
                    mTopDelta = thumbnailTop - screenLocation[1];

                    // Scale factors to make the large version the same size as the thumbnail
                    mWidthScale = (float) thumbnailWidth / iv_avatar_image.getWidth();
                    mHeightScale = (float) thumbnailHeight / iv_avatar_image.getHeight();

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) enterAnimation();

                    return true;
                }
            });
        }

        tv_heading.setText(contributorName);

        Glide.with(context).load(contributorAvatarUrl).error(R.drawable.avatar_image).placeholder(R.drawable.avatar_image)
                .into(iv_avatar_image);

        if (isInternetConnected(context)){
            new Atask().execute(contributorName);
        }

        iv_back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }

    public class Atask extends AsyncTask<String,Void,Void> {
        private ProgressDialog pDialog;
        boolean apiLimitExceeded = false;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(ContributorsDetailsActivity.this);
            pDialog.setMessage("Getting Data ...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            if(!pDialog.isShowing())
                pDialog.show();

        }

        @Override
        protected Void doInBackground(String... params) {
            HttpURLConnection urlConnection;
            URL url;
            InputStream inputStream;
            String response="";

            try{
                url = new URL("https://api.github.com/users/"+params[0]+"/repos");
                Log.e("url value", url.toString());
                urlConnection = (HttpURLConnection) url.openConnection();

                //set request type
                urlConnection.setRequestMethod("GET");

                urlConnection.setDoInput(true);
                urlConnection.connect();
                //check for HTTP response
                int httpStatus = urlConnection.getResponseCode();
                Log.e("httpstatus", "The response is: " + httpStatus);

                //if HTTP response is 200 i.e. HTTP_OK read inputstream else read errorstream
                if (httpStatus != HttpURLConnection.HTTP_OK) {
                    inputStream = urlConnection.getErrorStream();
                    Map<String, List<String>> map = urlConnection.getHeaderFields();
                    System.out.println("Printing Response Header...\n");
                    for (Map.Entry<String, List<String>> entry : map.entrySet()) {
                        System.out.println(entry.getKey()
                                + " : " + entry.getValue());
                    }
                }
                else {
                    inputStream = urlConnection.getInputStream();
                }

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
                String temp;
                while((temp = bufferedReader.readLine())!=null){
                    response+=temp;
                }
                Log.e("contributors repos",response);


                if(response.contains("API rate limit exceeded")){
                    apiLimitExceeded =true;
                }else {
                    //convert data string into JSONObject
                    JSONArray contributorsRepoArray = (JSONArray) new JSONTokener(response).nextValue();

                    for (int i=0; i<contributorsRepoArray.length(); i++){
                        JSONObject object = contributorsRepoArray.getJSONObject(i);
                        DataModel dataModel = new DataModel();
                        dataModel.setName(object.getString("name"));
                        dataModel.setFull_name(object.getString("full_name"));
                        dataModel.setWatchers_count(object.getInt("watchers_count"));
                        dataModel.setCommit_count(object.getString("forks_count"));
                        dataModel.setProject_url(object.getString("html_url"));
                        dataModel.setDescription(object.getString("description"));

                        JSONObject ownerjsonobject = object.getJSONObject("owner");
                        dataModel.setAvatar_url(ownerjsonobject.getString("avatar_url"));

                        contributorsRepoList.add(dataModel);
                    }

                }

                urlConnection.disconnect();
            } catch (MalformedURLException | ProtocolException | JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(!apiLimitExceeded){
                adapter = new ContributorRepoAdapter(context,contributorsRepoList);
                rv_contributors_repo.setAdapter(adapter);
            }
            if (pDialog.isShowing())
                pDialog.dismiss();
        }
    }

    public void enterAnimation() {

        // Set starting values for properties we're going to animate. These
        // values scale and position the full size version down to the thumbnail
        // size/location, from which we'll animate it back up
        iv_avatar_image.setPivotX(0);
        iv_avatar_image.setPivotY(0);
        iv_avatar_image.setScaleX(mWidthScale);
        iv_avatar_image.setScaleY(mHeightScale);
        iv_avatar_image.setTranslationX(mLeftDelta);
        iv_avatar_image.setTranslationY(mTopDelta);

        // interpolator where the rate of change starts out quickly and then decelerates.
        TimeInterpolator sDecelerator = new DecelerateInterpolator();

        // Animate scale and translation to go from thumbnail to full size
        iv_avatar_image.animate().setDuration(ANIM_DURATION).scaleX(1).scaleY(1).translationX(0).translationY(0).setInterpolator(sDecelerator);

        // Fade in the black background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(colorDrawable, "alpha", 0, 255);
        bgAnim.setDuration(ANIM_DURATION);
        bgAnim.start();
    }

    public void exitAnimation(final Runnable endAction) {
        TimeInterpolator sInterpolator = new AccelerateInterpolator();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            iv_avatar_image.animate().setDuration(ANIM_DURATION).scaleX(mWidthScale).scaleY(mHeightScale).
                    translationX(mLeftDelta).translationY(mTopDelta)
                    .setInterpolator(sInterpolator).withEndAction(endAction);
        }

        // Fade out background
        ObjectAnimator bgAnim = ObjectAnimator.ofInt(colorDrawable, "alpha", 0);
        bgAnim.setDuration(ANIM_DURATION);
        bgAnim.start();
    }

    public void onBackPressed(){
        //super.onBackPressed();
        //finish();
        //overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            exitAnimation(new Runnable() {
                public void run() {
                    finish();
                    overridePendingTransition(0, 0);
                }
            });
        }
        else {
            finish();
            overridePendingTransition(0, 0);
        }
    }
}
