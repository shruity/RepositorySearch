package com.repositorysearch;

public class DataModel {

	private String id;
    private String avatar_url;
    private String project_url, description;
    private String name;
    private String full_name;
    private int watchers_count;
    private String commit_count, contributor_name, contributor_avatar_url;

    public DataModel(){

    }
    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

	public String getAvatar_url() {
        return avatar_url;
    }
    public void setAvatar_url(String avatar_url) {
        this.avatar_url = avatar_url;
    }

    public String getProject_url() {
        return project_url;
    }
    public void setProject_url(String project_url) {
        this.project_url = project_url;
    }

    public String getDescription() {
        return description;
    }
    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getFull_name() {
        return full_name;
    }
    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public int getWatchers_count() {
        return watchers_count;
    }
    public void setWatchers_count(int watchers_count) {
        this.watchers_count = watchers_count;
    }

    public String getCommit_count() {
        return commit_count;
    }
    public void setCommit_count(String commit_count) {
        this.commit_count = commit_count;
    }

    public String getContributor_name() {
        return contributor_name;
    }
    public void setContributor_name(String contributor_name) {
        this.contributor_name = contributor_name;
    }

    public String getContributor_avatar_url() {
        return contributor_avatar_url;
    }
    public void setContributor_avatar_url(String contributor_avatar_url) {
        this.contributor_avatar_url = contributor_avatar_url;
    }


}
