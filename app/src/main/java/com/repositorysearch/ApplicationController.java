package com.repositorysearch;

import android.content.Context;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

public class ApplicationController extends MultiDexApplication {

	@Override
	public void onCreate() {
		super.onCreate();

	}
	@Override
	protected void attachBaseContext(Context newBase) {
		super.attachBaseContext(newBase);
		MultiDex.install(this);
	}
}